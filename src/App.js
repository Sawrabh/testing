import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import HttpInterface from "./interfaces/http.interface";

import './App.css';

const searchParameters = {
  title:"",
  url:"",
  author:""
}
class App extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
      original_page_data: [],
      filtered_page_data: [],
      page_size : 0,
      showModal: false,
      selectedHit : "",
      show_filtered_data: false,
      created_at_filter_val: false, //false for ascending
      title_filter_val: false,
      searchFields: Object.keys(searchParameters),
      searchFieldInput: searchParameters,
      searchedFromInputField: false
    }
  }

  searchData = (page_size) => {
    HttpInterface.searchFlight(page_size).then((successObj) => {
      console.log(successObj,"app.js");
      var pageData = [...this.state.original_page_data];
      for (var loop = 0; loop < successObj.data.hits.length; loop++) {
        successObj.data.hits[loop].created_at = this.formatDateToUTC(successObj.data.hits[loop].created_at);
        pageData.push(successObj.data.hits[loop]);
      }
      this.setState({
        original_page_data : pageData
      },() => {
        if(this.state.title_filter_val) {
          this.filterBasedOnColumnHeader('title');
        } else if(this.state.created_at_filter_val){
          this.filterBasedOnColumnHeader('created_at')
        } else if(this.state.searchedFromInputField) {
          if(this.state.searchFieldInput.author) {
            this.searchFromInput('author');
          } else if(this.state.searchFieldInput.title) {
            this.searchFromInput('title');
          } else {
            this.searchFromInput('url');
          }
        } else {
          this.setState({
            filtered_page_data: !this.state.show_filtered_data ? pageData : this.state.filtered_page_data
          });
        }
      });
    });
  }

  initializeInterval = () => {
    console.log('interval initialized');
    this.searchInterval = setInterval(() => {
      this.setState({
        page_size: this.state.page_size+1
      },() => {
        this.searchData(this.state.page_size);
      });
    },10000);
  }

  clearInitializedInterval = () => {
    console.log('interval cleared');
    clearInterval(this.searchInterval);
  }

  componentDidMount() {
    document.addEventListener("scroll",this.trackScrolling);
    console.log(this.state.page_size,'initial');
    this.searchData(this.state.page_size);
    this.initializeInterval();
  }

  componentWillUnmount () {
    this.clearInitializedInterval();
  }


  trackScrolling = () => {
    const wrappedElement = document.getElementById('root');
    if (this.isBottom(wrappedElement)) {
      console.log('bottom reached');
      this.clearInitializedInterval();
      this.setState({
        page_size: this.state.page_size+1
      },() => {
        this.searchData(this.state.page_size);
        this.initializeInterval();
      });
    }
  };

  isBottom = (el) =>  {
    return el.getBoundingClientRect().bottom <= window.innerHeight;
  }

  filterBasedOnColumnHeader = (filter) => {
    var filteredArray = [...this.state.original_page_data];
    let searcFieldsInput = this.state.searchFieldInput;
    searcFieldsInput.author = "";
    searcFieldsInput.title = "";
    searcFieldsInput.url = "";
    if(filter==="title") {
      filteredArray.sort((a,b) => this.state.title_filter_val ? a.title.localeCompare(b.title): b.title.localeCompare(a.title));
      this.setState({
        title_filter_val: !this.state.title_filter_val,
        created_at_filter_val: false,
        searchedFromInputField: false,
        
      });
    } else if (filter === "created_at") {
      filteredArray.sort((a,b) => this.state.created_at_filter_val ? a.created_at.localeCompare(b.created_at): b.created_at.localeCompare(a.created_at));
      this.setState({
        created_at_filter_val: !this.state.created_at_filter_val,
        title_filter_val: false,
        searchedFromInputField: false
      });
    }
    this.setState({
      filtered_page_data: filteredArray,
      searcFieldsInput: searcFieldsInput
    });
  }

  searchFromInput = (searchData) => {
    var filteredArray = [...this.state.original_page_data];
    var newArray = filteredArray.filter((hitdata) => {
      if(searchData === "title") {
        return hitdata.title ? hitdata.title.toLowerCase().includes(this.state.searchFieldInput.title.toLowerCase()):null;
      } else if (searchData === "url") {
        return hitdata.url ? hitdata.url.toLowerCase().includes(this.state.searchFieldInput.url.toLowerCase()):null;
      } else if (searchData === "author") {
        return hitdata.author ? hitdata.author.toLowerCase().includes(this.state.searchFieldInput.author.toLowerCase()):null;
      } else {
        return hitdata;
      }
    });
    
    this.setState({
      filtered_page_data: newArray,
      searchedFromInputField: true
    });
  }

  handleChangeForInput = (evt) => {
    let searcFieldsInput = this.state.searchFieldInput;
    searcFieldsInput.author = "";
    searcFieldsInput.title = "";
    searcFieldsInput.url = "";
    searcFieldsInput[evt.target.name] = evt.target.value;
    let name = evt.target.name;
    this.setState({
      searchFieldInput: searcFieldsInput,
      searchedFromInputField: true
    },() => {
        console.log(this.state.searchFieldInput);
        this.searchFromInput(name);
    });
  }
  
  handleCloseModal = () => {
    this.setState({
      showModal: false
    });
  }

  handleOpenModal = (hitData) => {
    this.setState({
      showModal: true,
      selectedHit: JSON.stringify(hitData)
    });
  }

  formatDateToUTC = (string) => {
    return new Date(string).toUTCString();
  }

  render () {
    return (
      <div className="container-fluid">
        <header className="App-header">
          <div className="row">
            <div className="col-12 table-header">
              <div className="row">
                <div className="col-3">
                  <div className="row">
                    <div className="col-12">
                      <input id='searchbasedOnTitle' name={this.state.searchFields[0]} 
                        type="text" className="form-control" 
                        placeholder="search by title" value={this.state.searchFieldInput.title} 
                        onChange={this.handleChangeForInput}/>
                    </div>
                  </div>
                </div>
                <div className="col-3">
                  <div className="row">
                    <div className="col-12">
                      <input id='searchbasedOnURL' name={this.state.searchFields[1]} 
                        type="text" className="form-control" 
                        placeholder="search by Url" value={this.state.searchFieldInput.url} 
                        onChange={this.handleChangeForInput}/>
                    </div>
                  </div>
                </div>
                <div className="col-3">
                  <div className="row">
                    <div className="col-12">
                      <input id='searchbasedOnAuthor' name={this.state.searchFields[2]} 
                        type="text" className="form-control" 
                        placeholder="search by author" value={this.state.searchFieldInput.author} 
                        onChange={this.handleChangeForInput}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div className="row default-margin-top">
          <div className="col-12 table-header">
            <div className="row">  
              <div className="col-3" onClick={this.filterBasedOnColumnHeader.bind(this,'title')}>Title {this.state.title_filter_val?"↑":"↓"}</div>
              <div className="col-3">URL</div>
              <div className="col-3">Author</div>
              <div className="col-3" onClick={this.filterBasedOnColumnHeader.bind(this,'created_at')}>Created_at {this.state.created_at_filter_val?"↑":"↓"}</div>
            </div>
          </div>
          <div className="col-12">
            {
              this.state.filtered_page_data.length? this.state.filtered_page_data.map((hitData,index) => {
                return (
                  <div key={index} className="row row-style" onClick={this.handleOpenModal.bind(this,hitData)}>
                     <div className="col-3">{hitData.title}</div>
                      <div className="col-3">{hitData.url}</div>
                      <div className="col-3">{hitData.author}</div>
                      <div className="col-3">{hitData.created_at}</div>
                  </div>
                );
              }):""
            }    
          </div>
        </div>
        <Modal size="lg" show={this.state.showModal} onHide={this.handleCloseModal}>
          <Modal.Header closeButton>
            <Modal.Title>Post</Modal.Title>
          </Modal.Header>
          <Modal.Body className="txt-overflow">{this.state.selectedHit}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default App;