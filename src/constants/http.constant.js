const HttpUrl = {
    baseUrl: "https://hn.algolia.com/api/v1/",
    search_by_date: "search_by_date?tags=story&page="
}

export default HttpUrl;