import axios from "axios";
import HttpUrl from "../constants/http.constant";

const AxiosConfig = axios.create({
  base_url: HttpUrl.baseUrl,
  headers: {'Content-Type': 'application/json'}
});

const HttpInterface = {
    searchFlight: function(page_size) {
        return AxiosConfig.get(HttpUrl.baseUrl+HttpUrl.search_by_date+page_size)
    }
}

export default HttpInterface;